 using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Rextester
{
    public class Program
    {
        public static void Main(string[] args)
        {
            double a, b;
            char z;
            Console.WriteLine("Write a ");
            a = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Operator (+, -, *, /) ");
            z=Convert.ToChar(Console.ReadLine());
            Console.WriteLine("Write b ");
            b = Convert.ToDouble(Console.ReadLine());
            switch (z)
            {
                case '+':
                    Console.WriteLine("{0}+{1}={2}", a, b, a + b);
                    break;
                case '-':
                Console.WriteLine("{0}-{1}={2}", a, b, a - b);
                    break;
                case '*':
                    Console.WriteLine("{0}*{1}={2}", a, b, a * b);
                    break;
                case '/':
                    Console.WriteLine("{0}/{1}={2}", a, b, a / b);
                    break;
                default: Console.WriteLine("Error");
                    break;
            }
            Console.ReadLine(); 
        }
    }
}